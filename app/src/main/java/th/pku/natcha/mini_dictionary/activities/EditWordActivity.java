package th.pku.natcha.mini_dictionary.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Collections;

import th.pku.natcha.mini_dictionary.R;
import th.pku.natcha.mini_dictionary.models.Storage;
import th.pku.natcha.mini_dictionary.models.Word;

public class EditWordActivity extends AppCompatActivity {
    Word oldWord;
    private TextView newTranslaion;
    private Button editSaveButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_word);
        oldWord = (Word)getIntent().getSerializableExtra("old word");
        initComponent();

    }
    private void initComponent(){

        newTranslaion = (TextView)findViewById(R.id.new_word_translation);
        editSaveButton = (Button)findViewById(R.id.edit_save_button);
        editSaveButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {;
                saveNewWord();
                Collections.sort(Storage.savedWords, new Word.AlphabetComparator());
                MainActivity.wordAdapter.notifyDataSetChanged();
                finish();
            }
        });

    }

    private void saveNewWord() {
        Storage.getInstance().editWord(oldWord, newTranslaion.getText().toString());
    }
}
