package th.pku.natcha.mini_dictionary.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import th.pku.natcha.mini_dictionary.R;
import th.pku.natcha.mini_dictionary.adapter.WordAdapter;
import th.pku.natcha.mini_dictionary.models.Storage;
import th.pku.natcha.mini_dictionary.models.Word;

public class MainActivity extends AppCompatActivity {
    private GridView wordsGridView;
    public static List<Word> words;
    public static List<String> wordsString;
    private AutoCompleteTextView AutoComTextView;
    public static WordAdapter wordAdapter;
    public static ArrayAdapter<String> adapter;
    private Button createButton,clearButton,searchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
    }
    public void initComponents(){
        words = new ArrayList<Word>();
        wordsString = new ArrayList<String>();
        wordAdapter = new WordAdapter(this,R.layout.list_cell,words);

        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, wordsString);
        AutoComTextView = (AutoCompleteTextView)findViewById(R.id.autocomplete_view);
        AutoComTextView.setAdapter(adapter);


        wordsGridView = (GridView)findViewById(R.id.words_grid_view);
        wordsGridView.setAdapter(wordAdapter);
        wordsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                Intent intent = new Intent(MainActivity.this, WordActivity.class);
                intent.putExtra("word", words.get(i));
                startActivity(intent);
            }
        });
        createButton = (Button)findViewById(R.id.add_button);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NewWordActivity.class);
                startActivity(intent);
            }
        });
        clearButton = (Button)findViewById(R.id.clear_button);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Storage.getInstance().clearWords();
                wordsString.clear();
                refreshWords();
            }
        });

        searchButton = (Button)findViewById(R.id.glass);
        searchButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, WordActivity.class);
                intent.putExtra("word", Storage.getInstance().findWord(AutoComTextView.getText().toString()));
                startActivity(intent);
            }
        });
    }
    public static void refreshWords(){
        words.clear();
        for(Word word: Storage.getInstance().loadWord()){
            words.add(word);
        }
        wordAdapter.notifyDataSetChanged();
    }


    @Override
    protected void onStart(){
        super.onStart();
        refreshWords();
    }
    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
    }
}
