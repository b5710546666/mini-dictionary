package th.pku.natcha.mini_dictionary.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import th.pku.natcha.mini_dictionary.R;
import th.pku.natcha.mini_dictionary.models.Word;

/**
 * Created by mind on 02/03/2016.
 */
public class WordAdapter extends ArrayAdapter<Word> {
    public WordAdapter(Context context, int resource, List<Word> objects){
        super(context,resource,objects);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        /////// 1st part
        View v = convertView;
        if(v == null){
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_cell,null);
        }
//        /////// 2nd part
        TextView name = (TextView)v.findViewById(R.id.word_name);

        Word word = getItem(position);
        name.setText(word.getName());
        return v;
    }
}
