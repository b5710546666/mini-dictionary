package th.pku.natcha.mini_dictionary.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import th.pku.natcha.mini_dictionary.R;
import th.pku.natcha.mini_dictionary.adapter.SynAntAdapter;
import th.pku.natcha.mini_dictionary.models.CheckAS;
import th.pku.natcha.mini_dictionary.models.Storage;
import th.pku.natcha.mini_dictionary.models.Word;

public class WordActivity extends AppCompatActivity {
    private Word name;
    private Button deleteButton,editButton;
    private TextView subject;
    private TextView body;
    private RelativeLayout relative;
    private Button add_syn , delete_syn , add_ant , delete_ant;
    private GridView synGridView , antGridView;
    public static SynAntAdapter synAdapter , antAdapter;
    public int [] check  = new int[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        name = (Word)getIntent().getSerializableExtra("word");
        setContentView(R.layout.activity_word);
        initComponets();
    }

    private void initComponets(){

        synAdapter = new SynAntAdapter(this,R.layout.list_cell2,name.getSynWords());
        antAdapter = new SynAntAdapter(this,R.layout.list_cell2,name.getAntWords());

        subject = (TextView)findViewById(R.id.subject);
        body = (TextView)findViewById(R.id.body);
        relative = (RelativeLayout)findViewById(R.id.relative);
        deleteButton = (Button)findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Storage.getInstance().deleteWord(name);
                MainActivity.wordsString.remove(name.getName());
                finish();
            }
        });
 
        editButton = (Button)findViewById(R.id.edit_button);
        editButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WordActivity.this, EditWordActivity.class);
                intent.putExtra("old word", name);
                startActivity(intent);
            }
        });

        ///////////////////////////////////////////////////////////////

        View.OnClickListener selectWord1 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WordActivity.this, Select_word.class);
                check[0] = (int)Storage.getInstance().loadWord().indexOf(name);
                check[1] = 0;
                intent.putExtra("thisWord",check);
                startActivity(intent);
            }
        };

        View.OnClickListener selectWord2 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WordActivity.this, Select_word.class);
                check[0] = (int)Storage.getInstance().loadWord().indexOf(name);
                check[1] = 1;
                intent.putExtra("thisWord",check);
                startActivity(intent);
            }
        };


        add_syn = (Button) findViewById(R.id.add_syn_button);
        add_syn.setOnClickListener(selectWord1);
        add_ant = (Button) findViewById(R.id.add_ant_button);
        add_ant.setOnClickListener(selectWord2);

        //////////////////////////////////////////////////////////////////////

        delete_syn = (Button) findViewById(R.id.delete_syn_button);
        delete_syn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WordActivity.this, DeleteSynAnt.class);
                CheckAS check = new CheckAS(name,"syn");
                intent.putExtra("delete",check);
                startActivity(intent);

            }
        });


        delete_ant = (Button) findViewById(R.id.delete_ant_button);
        delete_ant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WordActivity.this, DeleteSynAnt.class);
                CheckAS check = new CheckAS(name,"ant");
                intent.putExtra("delete",check);
                startActivity(intent);

            }
        });

        synGridView = (GridView) findViewById(R.id.syn_grid);
        synGridView.setAdapter(synAdapter);
        synGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                Intent intent = new Intent(WordActivity.this, WordActivity.class);

                intent.putExtra("word", Storage.getInstance().findWord(name.getSynWords().get(i)));
                startActivity(intent);
            }
        });

        antGridView = (GridView) findViewById(R.id.ant_grid);
        antGridView.setAdapter(antAdapter);
        antGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                Intent intent = new Intent(WordActivity.this, WordActivity.class);
                intent.putExtra("word", Storage.getInstance().findWord(name.getAntWords().get(i)));
                startActivity(intent);
            }
        });

    }


    @Override
    protected void onStart(){
        super.onStart();
        subject.setText(name.getName());
        body.setText(name.getTranslation());
    }
    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        name = Storage.getInstance().findWord(name.getName());
        synAdapter = new SynAntAdapter(this,R.layout.list_cell,name.getSynWords());
        antAdapter = new SynAntAdapter(this,R.layout.list_cell,name.getAntWords());
        synGridView.invalidateViews();
        synGridView.setAdapter(synAdapter);
        antGridView.invalidateViews();
        antGridView.setAdapter(antAdapter);
    }
}