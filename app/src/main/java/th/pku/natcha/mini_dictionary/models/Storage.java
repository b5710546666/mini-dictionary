package th.pku.natcha.mini_dictionary.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mind on 25/02/2016.
 */
public class Storage {
    public static List<Word> savedWords;
    private static Storage instance;
    private Storage(){

        savedWords = new ArrayList<Word>();
//        Word test = new Word("A","AAA");
//        test.addSyn("aaa");
//        test.addSyn("bbb");
//        Word a = new Word("aaa","testA");
//        Word b = new Word("bbb","testB");
//        savedWords.add(test);
//        savedWords.add(a);
//        savedWords.add(b);
//
//
//        Word test2 = new Word("C","CCC");
//        test2.addAnt("ccc");
//        test2.addAnt("ddd");
//        Word c = new Word("ccc","testC");
//        Word d = new Word("d","testD");
//        savedWords.add(test2);
//        savedWords.add(c);
//        savedWords.add(d);

    }
    public static Storage getInstance(){
        if(instance == null){
            instance = new Storage();
        }
        return instance;
    }

    public void saveWord(Word word){
        savedWords.add(word);
    }
    public List<Word> loadWord(){
        return savedWords;
    }
    public void clearWords(){
        savedWords.clear();
    }
    public void deleteWord(Word word){
        savedWords.remove(word);
    }
    public void editWord(Word oldWord,String newTranslation){
        int index = savedWords.indexOf(oldWord);
        Word w = savedWords.get(index);
        w.setTranslation(newTranslation);
        savedWords.set(index,w);
    }
    public Word findWord(String word){
        for(int i = 0;i<savedWords.size();i++){
            if(savedWords.get(i).getName().equals(word)) return savedWords.get(i);
        }
        return null;
    }
}