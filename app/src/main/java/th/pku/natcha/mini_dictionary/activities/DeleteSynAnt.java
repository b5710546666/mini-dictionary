package th.pku.natcha.mini_dictionary.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import th.pku.natcha.mini_dictionary.R;
import th.pku.natcha.mini_dictionary.adapter.SynAntAdapter;
import th.pku.natcha.mini_dictionary.models.CheckAS;
import th.pku.natcha.mini_dictionary.models.Storage;

public class DeleteSynAnt extends AppCompatActivity {
    private GridView wordListGrid;
    private TextView header;
    private CheckAS check;
    private List<String> words,deleteWords;
    private SynAntAdapter adapter;
    private List<Boolean> click;
    private Button delBut;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_syn_ant);
        check = (CheckAS)getIntent().getSerializableExtra("delete");
        initComponent();
    }
    public void initComponent(){
        click = new ArrayList<Boolean>();
        delBut = (Button)findViewById(R.id.delete_button);
        deleteWords = new ArrayList<String>();
        wordListGrid = (GridView)findViewById(R.id.synAnt_grid_view);
        header = (TextView)findViewById(R.id.head);
        if(check.getCheck().equals("syn")) words = check.getWord().getSynWords();
        else words = check.getWord().getAntWords();
        adapter = new SynAntAdapter(this,R.layout.list_cell,words);

        for(int i = 0;i<words.size();i++){
            click.add(false);
        }

        wordListGrid.setAdapter(adapter);
        wordListGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                if (!click.get(i)) {
                    view.setBackgroundColor(Color.LTGRAY);
                    deleteWords.add(words.get(i));
                    click.set(i,true);
                } else {
                    view.setBackgroundColor(Color.TRANSPARENT);
                    deleteWords.remove(words.get(i));
                    click.set(i,false);
                }

            }
        });
        delBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check.getCheck().equals("syn")){
                    Storage.getInstance().loadWord().get(
                            Storage.getInstance().loadWord().indexOf(check.getWord())
                    ).deleteSyn(deleteWords);

                    for (int i = 0 ; i < deleteWords.size(); i++){
                        Storage.getInstance().loadWord().get(
                                Storage.getInstance().loadWord().indexOf(Storage.getInstance().findWord(deleteWords.get(i)))
                        ).deleteSyn(check.getWord().getName());
                    }

                }else{
                    Storage.getInstance().loadWord().get(
                            Storage.getInstance().loadWord().indexOf(check.getWord())
                    ).deleteAnt(deleteWords);

                    for (int i = 0 ; i < deleteWords.size(); i++){
                        Storage.getInstance().loadWord().get(
                                Storage.getInstance().loadWord().indexOf(Storage.getInstance().findWord(deleteWords.get(i)))
                        ).deleteAnt(check.getWord().getName());
                    }
                }
                finish();
            }
        });

    }
}