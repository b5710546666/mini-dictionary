
package th.pku.natcha.mini_dictionary.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import th.pku.natcha.mini_dictionary.R;
import th.pku.natcha.mini_dictionary.adapter.WordAdapter;
import th.pku.natcha.mini_dictionary.models.Storage;
import th.pku.natcha.mini_dictionary.models.Word;

public class Select_word extends AppCompatActivity {
    // + button
    private Button addWords,choose;

    private GridView listWord;
    public static List<Word> words;
    public static List<String> wordsString;
    private AutoCompleteTextView typeField;
    public static WordAdapter wordAdapter;
    public static ArrayAdapter<String> adapter;
    public int[] thisWordIndex ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_word);
        thisWordIndex = (int[]) getIntent().getSerializableExtra("thisWord");
        initComponents();
    }

    public void initComponents(){
        addWords = (Button)findViewById(R.id.add_button);
        addWords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Select_word.this, NewWordActivity.class);
                intent.putExtra("word", Storage.getInstance().findWord(typeField.getText().toString()));
                startActivity(intent);
            }
        });

        words = Storage.getInstance().loadWord();
        wordsString = new ArrayList<String>();
        wordsString = MainActivity.wordsString;
        wordAdapter = new WordAdapter(this,R.layout.list_cell,words);

        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, wordsString);
        typeField = (AutoCompleteTextView)findViewById(R.id.typeField_view);
        typeField.setAdapter(adapter);

        listWord = (GridView)findViewById(R.id.listWord_view);
        listWord.setAdapter(wordAdapter);
        listWord.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                if (thisWordIndex[1] == 0) {
                    Storage.getInstance().loadWord().get(thisWordIndex[0]).addSyn(words.get(i).getName());
                    Storage.getInstance().loadWord().get(Storage.getInstance().loadWord().indexOf(words.get(i))).addSyn(Storage.getInstance().loadWord().get(thisWordIndex[0]).getName());
                } else {
                    Storage.getInstance().loadWord().get(thisWordIndex[0]).addAnt(words.get(i).getName());
                    Storage.getInstance().loadWord().get(Storage.getInstance().loadWord().indexOf(words.get(i))).addAnt(Storage.getInstance().loadWord().get(thisWordIndex[0]).getName());
                }
                finish();
            }
        });
        choose = (Button)findViewById(R.id.choose_button);
        choose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (thisWordIndex[1] == 0) {
                    Storage.getInstance().loadWord().get(thisWordIndex[0]).addSyn(
                            Storage.getInstance().loadWord().get(
                                    Storage.getInstance().loadWord().indexOf(
                                            Storage.getInstance().findWord(
                                                    typeField.getText().toString()
                                            )
                                    )
                            ).getName()
                    );
                    Storage.getInstance().loadWord().get(
                            Storage.getInstance().loadWord().indexOf(
                                    Storage.getInstance().findWord(typeField.getText().toString())
                            )
                    ).addSyn(Storage.getInstance().loadWord().get(thisWordIndex[0]).getName());
                } else {
                    Storage.getInstance().loadWord().get(thisWordIndex[0]).addAnt(
                            Storage.getInstance().loadWord().get(
                                    Storage.getInstance().loadWord().indexOf(
                                            Storage.getInstance().findWord(
                                                    typeField.getText().toString()
                                            )
                                    )
                            ).getName()
                    );
                    Storage.getInstance().loadWord().get(
                            Storage.getInstance().loadWord().indexOf(
                                    Storage.getInstance().findWord(typeField.getText().toString())
                            )
                    ).addAnt(Storage.getInstance().loadWord().get(thisWordIndex[0]).getName());
                }
               finish();
            }
        });


    }
}
