package th.pku.natcha.mini_dictionary.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import th.pku.natcha.mini_dictionary.R;

/**
 * Created by makham on 28/3/2559.
 */
public class SynAntAdapter extends ArrayAdapter<String> {
    public SynAntAdapter(Context context, int resource, List<String> objects){
        super(context,resource,objects);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        View v = convertView;
        if(v == null){
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_cell,null);
        }
        TextView name = (TextView)v.findViewById(R.id.word_name);

        String word = getItem(position);
        name.setText(word);
        return v;
    }


}
