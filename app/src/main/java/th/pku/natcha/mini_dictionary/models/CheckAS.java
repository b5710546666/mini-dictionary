package th.pku.natcha.mini_dictionary.models;

import java.io.Serializable;

/**
 * Created by mind on 29/03/2016.
 */
public class CheckAS implements Serializable {
    private  Word w;
    private String check;
    public CheckAS(Word w,String check){
        this.w = w;
        this.check=check;
    }
    public Word getWord(){return this.w;}
    public String getCheck(){return this.check;}
}
