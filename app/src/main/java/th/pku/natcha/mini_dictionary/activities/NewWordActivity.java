package th.pku.natcha.mini_dictionary.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Collections;

import th.pku.natcha.mini_dictionary.R;
import th.pku.natcha.mini_dictionary.models.Storage;
import th.pku.natcha.mini_dictionary.models.Word;

public class NewWordActivity extends AppCompatActivity {
    private TextView newWord;
    private TextView newTranslaion;
    private Button saveButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_word);
        initComponents();
    }
    private void initComponents(){
        newWord = (TextView)findViewById(R.id.new_word_name);
        newTranslaion = (TextView)findViewById(R.id.new_word_translation);
        saveButton = (Button)findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveNewWord();
                Collections.sort(Storage.savedWords, new Word.AlphabetComparator());
                MainActivity.wordAdapter.notifyDataSetChanged();
                MainActivity.adapter.notifyDataSetChanged();
                finish();
            }
        });
    }
    private void saveNewWord() {
        Storage.getInstance().saveWord(
                new Word(newWord.getText().toString(),
                        newTranslaion.getText().toString()));
        MainActivity.wordsString.add(newWord.getText().toString());
        MainActivity.refreshWords();
    }
}
