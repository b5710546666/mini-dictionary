package th.pku.natcha.mini_dictionary.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by mind on 02/03/2016.
 */
public class Word implements Serializable {
    private String name;
    private String translation;
    public List<String> synWords;
    public List<String> antWords;

    public Word(String name,String translation){
        this.name = name;
        this.translation = translation;
        synWords = new ArrayList<String>();
        antWords = new ArrayList<String>();
    }
    public String getName(){
        return this.name;
    }
    public String getTranslation(){
        return this.translation;
    }
    public void setName(String newName){
        this.name = newName;
    }
    public void setTranslation(String newTranslation){
        this.translation = newTranslation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Word word = (Word) o;
        return name.equals(word.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public static class AlphabetComparator implements Comparator<Word> {
        @Override
        public int compare(Word word1, Word word2) {
            return word1.getName().compareTo(word2.getName());
        }
    }

    public void addSyn(String s){ synWords.add(s); }
    public void addAnt(String s){ antWords.add(s); }

    public List<String> getSynWords(){ return  synWords ;}
    public List<String> getAntWords(){ return  antWords ;}

    public void deleteSyn(List<String> s){
        for(int i =0;i<s.size();i++){
            synWords.remove(s.get(i));
        }
    }
    public void deleteAnt(List<String> s){
        for(int i =0;i<s.size();i++){
            antWords.remove(s.get(i));
        }
    }

    public void deleteSyn(String s){
        synWords.remove(s);
    }

    public void deleteAnt(String s){
        antWords.remove(s);
    }


}