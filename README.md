## **Iteration I** ##


Use Case1 : add new word

Version: 0.1

Primary Actor: User

Main Success Scenario 

 1. User enters the application 
 
 2. User arrives at the main page 
 
 3. User press “+” button to create new word
 
 4. The word Display on the list in main page.


Use Case2 : add translation

Version: 0.1

Primary Actor: User

Main Success Scenario 

1. User press “+” button and input word

2. User input the translation.


Use Case3 : list all added words in a single page 

Version: 0.1

Primary Actor: User

Main Success Scenario 

1. User enter the application in to  main page

2. main page display the list of word that were added.


Use Case4 : click on each word to see a translation 


Version: 0.1

Primary Actor: User

Main Success Scenario 

1. User enter the application in to  main page

2. main page display the list of word that were added.

3. User click on each of word that show on the list to see translation.




## **Iteration II** ##


Use Case5 : Delete translation of each word

Version: 0.1

Primary Actor: User

Main Success Scenario 

1. User enters the application. 

2. Main page display the list of word that were added.

3. User select a word from the list. 

4. Translation page display the translation , edit button and delete button.

5. User click at ‘Edit button’.

6. The pop-up prompt  to edit or remove this word translation. 


Use Case6 : delete all words

Version: 0.1

Primary Actor: User

Main Success Scenario 

1. User enters the application. 

2. Main page display the list of word that were added.

3. User click at ‘Clear button’.

4. All recorded words are delete.


Use Case7 : delete individual word (with its translation) 

Version: 0.1

Primary Actor: User

Main Success Scenario 

1. User enters the application.

2. Main page display the list of word that were added.

3. User select a word from the list. 

4. Translation page display the translation , edit button and delete button.

5. User click at ‘Delete button’.

6. This word is deleted. 


Use Case8 : search for word 

Version: 0.1

Primary Actor: User

Main Success Scenario 

1. User enters the application 

2. Main page display the list of word that were added.

3. User type a requiring word, then click magnifier symbol.


## **Iteration III** ##


Use Case9 : Add similar words to each word

Version: 0.1

Primary Actor: User

Main Success Scenario 

1. User enters the application. 

2. Main page display the list of word that were added.

3. User select a word from the list. 

4. Translation page display the translation , its synonym words with "add button".

5. User click add button (in synonym).

6. Main page display the list of word that were added.

7. User select synonym word.

8. The new synonym word will be added

Use Case10 : Jump to similar word screen from another word

Version: 0.1

Primary Actor: User

Main Success Scenario 

1. User enters the application. 

2. Main page display the list of word that were added.

3. User select a word from the list. 

4. Translation page display the translation , and its synonym words.

5. User click at similar words.

6. Translation of that similar words display.


Use Cas11 : Delete similar word from each word

Version: 0.1

Primary Actor: User

Main Success Scenario 

1. User enters the application. 

2. Main page display the list of word that were added.

3. User select a word from the list. 

4. Translation page display the translation , its synonym words with "add button" and "delete button".

5. User click at delete button.

6. List of synonym and delete button displays.

7. User select synonym words, they want to remove and click at delete button.

8. Synonym words are deleted.


Use Case12 : Add antonym word 

Version: 0.1

Primary Actor: User

Main Success Scenario 

1. User enters the application. 

2. Main page display the list of word that were added.

3. User select a word from the list. 

4. Translation page display the translation , its antonym words with "add button" and "delete button".

5. User click add button (in antonym).

6. Main page display the list of word that were added.

7. User select antonym word.

8. The new antonym word will be added